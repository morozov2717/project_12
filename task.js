function func_1()
{
    return new Promise((resolve)=>{
        setTimeout(()=>{
            resolve()
            console.log('Запрос обработан успешно!')
        }, 2000)
    })
}
const url = 'https://jsonplaceholder.typicode.com/todos'
    async function func_2(url){
        await func_1()
        const response = await fetch(url) 
        .then(response => response.json())
        .then(data => console.log(data))
    }
    func_2(url)